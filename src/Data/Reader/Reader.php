<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */

namespace Ext\Data\Reader;

use Ext\AbstractBase;

/**
 * Class Reader
 * @package Ext\Data\Reader
 */
class Reader extends AbstractBase
{
    protected $type = 'reader';
}
