<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Data\Proxy;

/**
 * Class Ajax
 * @package Ext\Data\Proxy
 */
class Ajax extends Server
{
    /** @var string */
    protected $type = 'ajax';
}
