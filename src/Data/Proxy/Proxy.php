<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Data\Proxy;

use Ext\AbstractBase;

/**
 * Class Proxy
 * @package Ext\Data\Proxy
 */
class Proxy extends AbstractBase
{
    /** @var string */
    protected $type = 'proxy';

    /**
     * @return array
     */
    public function encode()
    {
        /** @var array $data */
        $data = parent::encode();

        if (! isset($data['type'])) {
            $data['type'] = $this->type;
        }

        return $data;
    }
}
