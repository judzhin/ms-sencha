<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Data\Proxy;

/**
 * Class Server
 * @package Ext\Data\Proxy
 */
class Server extends Proxy
{
    /** @var string */
    protected $type = 'server';
}
