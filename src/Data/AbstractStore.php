<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Data;

use Ext\AbstractBase;

/**
 * Class AbstractStore
 * @package Ext\Data
 */
abstract class AbstractStore extends AbstractBase
{

}
