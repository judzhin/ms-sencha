<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Data;

use Ext\AbstractBase;
use Ext\Data\Field\Field;

/**
 * Class Model
 * @package Ext\Data
 */
class Model extends AbstractBase
{
    /**
     * @param array $instanceConfig
     * @return $this
     */
    public function initConfig(array $instanceConfig)
    {
        if (isset($instanceConfig['fields']) && is_array($instanceConfig['fields'])) {
            foreach ($instanceConfig['fields'] as $key => $field) {
                if (is_string($field)) {
                    $field = ['name' => $field];
                }

                if (is_array($field)) {
                    $instanceConfig['fields'][$key] = Field::factory($field);
                }
            }
        }

        return parent::initConfig($instanceConfig);
    }
}
