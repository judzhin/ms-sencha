<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Data\Field;

/**
 * Class Number
 * @package Ext\Data\Field
 */
class Number extends Integer
{
    /** @var string */
    protected $type = 'number';
}
