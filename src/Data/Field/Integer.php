<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Data\Field;

/**
 * Class Integer
 * @package Ext\Data\Field
 */
class Integer extends Field
{
    /** @var string */
    protected $type = 'integer';
}
