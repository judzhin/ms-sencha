<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Data\Field;

/**
 * Class Date
 * @package Ext\Data\Field
 */
class Date extends Field
{
    /** @var string */
    protected $type = 'date';
}
