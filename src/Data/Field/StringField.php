<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Data\Field;

/**
 * Class StringField
 * @package Ext\Data\Field
 */
class StringField extends Field
{
    /** @var string */
    protected $type = 'string';
}
