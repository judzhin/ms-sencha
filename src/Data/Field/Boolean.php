<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Data\Field;

/**
 * Class Boolean
 * @package Ext\Data\Field
 */
class Boolean extends Field
{
    /** @var string */
    protected $type = 'boolean';
}
