<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Data\Field;

use Ext\AbstractBase;

/**
 * Class Field
 * @package Ext\Data\Field
 */
class Field extends AbstractBase
{
    /** @var string */
    protected $type = 'auto';

    /**
     * @param array $options
     * @return Field
     */
    public static function factory(array $options)
    {
        $options['type'] = isset($options['type']) ?: 'auto';

        switch ($options['type']) {
            case 'bool':
            case 'boolean':
                return new Boolean($options);
                break;
            case 'date':
                return new Date($options);
                break;
            case 'int':
            case 'integer':
                return new Integer($options);
                break;
            case 'float':
            case 'number':
                return new Number($options);
                break;
            case 'string':
                return new StringField($options);
                break;
            case 'auto':
            default:
                return new self($options);
                break;
        }
    }
}
