<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Data;

/**
 * Class Store
 * @package Ext\Data
 */
class Store extends ProxyStore implements StoreInterface
{

}
