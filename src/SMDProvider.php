<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext;

/**
 * Class SMDProvider
 * @package Ext
 */
class SMDProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencies(),
            self::class => $this->getMappings(),
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            'invokables' => [

            ],
            'factories'  => [

            ],
        ];
    }

    /**
     * @return array
     */
    public function getMappings()
    {
        return [
            'Heroes' => [
                'methods' => [
                    'getResults' => [
                        'len' => 1
                    ]
                ]
            ]
        ];
    }
}
