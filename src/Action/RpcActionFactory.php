<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Action;

use Ext\SMDProvider;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class RpcActionFactory
 * @package Ext\Action
 */
class RpcActionFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return RpcAction
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new RpcAction(
            (new SMDProvider())->getMappings()
        );
    }
}
