<?php

/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Action;

use Ext\Rpc\Heroes;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Mockery\CountValidator\Exception;
use Zend\Diactoros\Response;
use Zend\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Expressive\Helper\UrlHelper;
use Zend\Json\Server\Server;
use Zend\Json\Server\Smd;

/**
 * Class RpcAction
 * @package Ext\Action
 */
class RpcAction implements ServerMiddlewareInterface
{
    /** @var array */
    protected $directMapping;

    /**
     * RpcAction constructor.
     * @param array $directMapping
     */
    public function __construct(array $directMapping)
    {
        $this->directMapping = $directMapping;
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        /** @var array $body */
        $body = $request->getParsedBody();

        /** @var string $action */
        $action = $body['action'];

        try {

            if (!isset($this->directMapping[$action])) {
                throw new \Exception('Call to undefined action: ' . $action);
            }

            /** @var string $a */
            $a = $this->directMapping[$action];

            /** @var string $method */
            $method = $body['method'];

            /** @var string $mdef */
            $mdef = $a['methods'][$method];

            if (!$mdef) {
                throw new \Exception(
                    'Call to undefined method: ' . $method . ' in action ' . $action
                );
            }

            /** @var array $data */
            $data = [
                'type' => 'rpc',
                'tid' => $body['tid'],
                'action' => $action,
                'method' => $method
            ];

            // $o = new \stdClass;
            $o = $this;

            if (isset($mdef['len'])) {
                $params = isset($body['data']) && is_array($body['data']) ? $body['data'] : [];
            } else {
                $params = [$body['data']];
            }

            $data['result'] = call_user_func_array([$o, $method], $params);

        } catch (\Exception $exc) {
            $data['type'] = 'exception';
            $data['message'] = $exc->getMessage();
            $data['where'] = $exc->getTraceAsString();
        }

        return new JsonResponse($data);
    }

    /**
     * @return array
     */
    public function getResults()
    {
        return [
            ['id' => 1],
            ['id' => 2],
            ['id' => 3],
        ];
    }
}
