<?php

/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Json\Encoder;
use Zend\Json\Server\Server;
use Zend\Json\Server\Smd;

/**
 * Class SmdAction
 * @package Ext\Action
 */
class SmdAction implements ServerMiddlewareInterface
{
    /** @var  array */
    protected $configProvider;

    /**
     * SmdAction constructor.
     * @param array $configProvider
     */
    public function __construct(array $configProvider)
    {
        $this->configProvider = $configProvider;
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        //$server = new Server();
        //$server->setClass($this);
        //$server->setTarget('/rpc.json')
        //    ->setEnvelope(Smd::ENV_JSONRPC_2);
        //echo $server->getServiceMap();
        //die();

        //try {
        //    /** @var Smd $smd */
        //    $smd = new Smd;
        //
        //    $service = new Smd\Service('some-spec');
        //    $smd->addService($service);
        //    $smd->setTransport('POST');
        //
        //    echo $smd;
        //} catch (\Exception $exc) {
        //    var_dump($exc->getMessage()); die();
        //}


        // die();
        // {
        // "transport": "POST",
        // "envelope": "JSON-RPC-2.0",
        // "contentType": "application/json",
        // "SMDVersion": "2.0",
        // "description": null,
        // "target": "/rpc.json",
        // "services": {
        // "process": {
        //     "envelope": "JSON-RPC-2.0",
        // "transport": "POST",
        // "name": "process",
        // "parameters": [
        //     {
        //        "type": "object",
        //        "name": "request",
        //        "optional": false
        //    },
        //    {
        //        "type": "object",
        //        "name": "delegate",
        //        "optional": false
        //    }
        //],
        //"returns": "null"
        //}
        //},
        //}
        //}
        //}

        header('Content-Type: text/javascript');

        // {
        //     "url":"/rpc.json",
        //     "type":"remoting",
        //     "actions":{
        //         "Heroes":[{
        //             "name":"getResults",
        //             "len":1
        //         }]
        //     }
        // }

        /** @var array $actions */
        $actions = [];
        $API = $this->configProvider;

        /**
         * @var  $aname
         * @var  $a
         */
        foreach ($API as $aname => $a) {

            /** @var array $methods */
            $methods = [];

            foreach ($a['methods'] as $mname => $m) {

                if (isset($m['len'])) {
                    $md = [
                        'name' => $mname,
                        'len' => $m['len']
                    ];
                } else {
                    $md = [
                        'name' => $mname,
                        'params' => $m['params']
                    ];
                }

                if (isset($m['formHandler']) && $m['formHandler']) {
                    $md['formHandler'] = true;
                }

                $methods[] = $md;
            }

            $actions[$aname] = $methods;
        }

        echo 'var Ext = Ext || {}; Ext.REMOTING_API = ';
        echo Encoder::encode([
            'url' => '/rpc.json',
            'type' => 'remoting',
            'actions' => $actions
        ]);
        echo ';';
        die();
    }
}