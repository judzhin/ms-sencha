<?php

/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Action;

use Ext\SMDProvider;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class SmdActionFactory
 * @package Ext\Action
 */
class SmdActionFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return SmdAction
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new SmdAction(
            (new SMDProvider())->getMappings()
        );
    }
}