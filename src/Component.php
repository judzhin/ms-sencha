<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext;

use Ext\Enums\Widget;

/**
 * Class Component
 * @package Ext
 */
class Component extends AbstractBase
{
    /**
     * @var string
     */
    protected $xtype = Widget::COMPONENT;

    /** @var Config */
    protected $initialConfig;

    /**
     * Component constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        /** @var Config initialConfig */
        $this->initialConfig = new Config($config);
        $this->initConfig($this->initialConfig);
    }

    /**
     *
     */
    protected function initComponent()
    {
    }

    /**
     * @param Config $config
     */
    protected function initConfig(Config $config)
    {
        // TODO: Implement initConfig() method.
    }

    /**
     * @param null $name
     * @return mixed
     */
    protected function getInitialConfig($name = null)
    {
        return $this->initialConfig->getData($name);
    }

    /**
     * @return array
     */
    public function encode()
    {
        /** @var array $data */
        $data = parent::encode();

        if (!isset($data['xtype'])) {
            $data['xtype'] = $this->xtype;
        }

        return $data;
    }
}
