<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Grid\Column;

use Ext\Grid\Header\Container;

/**
 * Class Column
 * @package Ext\Grid\Column
 */
class Column extends Container
{
    /**
     * @var string
     */
    protected $xtype = 'gridcolumn';

    /**
     * @param array $options
     * @return Column
     */
    public static function factory(array $options)
    {
        return new self($options);
    }
}
