<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Grid\Header;

use Ext\Container\Container as BaseContainer;

/**
 * Class Container
 * @package Ext\Grid\Header
 */
class Container extends BaseContainer
{

}
