<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Grid;

use Ext\Panel\Table;

/**
 * Class Panel
 * @package Ext\Grid
 */
class Panel extends Table
{
    /**
     * {@inheritDoc}
     *
     * @var string
     */
    protected $xtype = 'grid';
}
