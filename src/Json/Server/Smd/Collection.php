<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Json\Server\Smd;
use Ext\Json\Server\Server;

/**
 * Class Collection
 * @package Ext\Json\Server\Smd
 */
class Collection extends Server
{
    /** @var  string */
    protected $name;

    /** @var array */
    protected $services = [];

    /**
     * Collection constructor.
     * @param string $name
     */
    public function __construct($name = null)
    {
        $this->name = $name;
    }

    /**
     * @param string $class
     * @param string $namespace
     * @param null $argv
     * @return \Zend\Json\Server\Server
     */
    public function setClass($class, $namespace = '', $argv = null)
    {
        $this->name = $class;
        return parent::setClass($class, $namespace, $argv); // TODO: Change the autogenerated stub
    }

}