<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */

namespace Ext\Json\Server;

use Zend\Json\Server\Smd as DefaultSmd;

/**
 * Class Smd
 * @package Ext\Json\Server
 */
class Smd extends DefaultSmd
{

}