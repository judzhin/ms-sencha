<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Json\Server;

use Zend\Json\Server\Server as DefaultServer;

/**
 * Class Server
 * @package Ext\Json\Server
 */
class Server extends DefaultServer
{
    /** @var array */
    protected $collection = [];
}