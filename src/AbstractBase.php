<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */

namespace Ext;

/**
 * Class AbstractBase
 * @package Sencha
 */
abstract class AbstractBase implements BaseInterface
{
    /**
     * @param Config $config
     * @return mixed
     */
    protected abstract function initConfig(Config $config);

    /**
     * @param null $name
     * @return mixed
     */
    protected abstract function getInitialConfig($name = null);

//    /**
//     * AbstractBase constructor.
//     * @param array $config
//     */
//    public function __construct(array $config = [])
//    {
//        $this->initConfig($config);
//    }

    /**
     * @return string
     */
    final public function getClassName()
    {
        return static::class;
    }

//    /**
//     * @param array $instanceConfig
//     * @return $this
//     */
//    public function initConfig(array $instanceConfig)
//    {
//        return $this->addData($instanceConfig);
//    }

//    /**
//     * @param null $name
//     * @return array
//     */
//    public function getInitialConfig($name = null)
//    {
//        return $this->getData($name);
//    }

//    /**
//     * @return array
//     */
//    public function encode()
//    {
//        /** @var array $arr */
//        $arr = $this->getData();
//
//        /** @var FilterInterface $filter */
//        $filter = new UnderscoreToCamelCase();
//
//        /**
//         * @param $value
//         * @return array
//         */
//        $fnCallback = function ($value) {
//
//            if (is_array($value)) {
//
//                /**
//                 * @var int $key
//                 * @var mixed $data
//                 */
//                foreach ($value as $key => $data) {
//                    if ($data instanceof BaseInterface) {
//                        $value[$key] = $data->encode();
//                    }
//                }
//            } elseif ($value instanceof BaseInterface) {
//
//                /** @var array $value */
//                $value = $value->encode();
//            }
//
//            return $value;
//        };
//
//        /**
//         * @var string $oldKey
//         * @var mixed $value
//         */
//        foreach ($arr as $oldKey => $value) {
//
//            /** @var string $newKey */
//            $newKey = lcfirst($filter->filter($oldKey));
//            $arr[$newKey] = $fnCallback($value);
//
//            if ($newKey != $oldKey) {
//                unset($arr[$oldKey]);
//            }
//        }
//
//        return $arr;
//    }
}
