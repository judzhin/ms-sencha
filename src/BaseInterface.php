<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */

namespace Ext;

/**
 * Interface BaseInterface
 * @package Ext
 */
interface BaseInterface
{
    /**
     * @return mixed
     */
    public function encode();
}
