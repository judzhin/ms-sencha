<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext;

use Ext\App\Application;

/**
 * Class Ext
 * @package Ext
 */
abstract class Ext
{
    // /**
    //  * @param $className
    //  * @param $data
    //  * @param callable|null $callback
    //  */
    // public static function define($className, $data, callable $callback = null)
    // {
    //
    // }

    /**
     * @param $name
     * @param array|null $arguments
     * @return mixed
     */
    public static function create($name, array $arguments = null)
    {
        if (is_string($name)) {
            return new $name($arguments);
        }

    }

    /**
     * @param $config
     */
    public static function application($config)
    {
        echo (new Application())->launch();
    }
}