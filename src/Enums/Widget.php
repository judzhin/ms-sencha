<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Enums;

use MSBios\Stdlib\Enum;

/**
 * Class Widget
 * @package Ext\Enums
 */
abstract class Widget extends Enum
{
    /** @const  */
    const COMPONENT = 'component';
}
