<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbis.com>
 */
namespace Ext\App;
use Zend\Json\Json;

/**
 * Class Application
 * @package Ext\App
 */
class Application extends Controller
{
    /**
     * @param null $proflie
     * @return string
     */
    public function launch($proflie = null)
    {
        return Json::encode([]);
    }
}