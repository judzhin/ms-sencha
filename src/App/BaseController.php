<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\App;

use Ext\Base;

/**
 * Class BaseController
 * @package Ext\App
 */
class BaseController extends Base
{

}