<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */

namespace Ext\Exception;

/**
 * Class InvalidArgumentException
 * @package Ext\Exception
 */
class InvalidArgumentException extends \InvalidArgumentException
{

}
