<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext\Panel;

use Ext\Container\Container;

/**
 * Class Panel
 * @package Ext\Panel
 */
class Panel extends Container
{
    /** @var string */
    protected $xtype = 'panel';
}
