<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
namespace Ext;

use MSBios\Stdlib\Object;

/**
 * Class Config
 * @package Ext
 */
class Config extends Object
{
    /**
     * Config constructor.
     * @param $arr
     */
    public function __construct($arr)
    {
        parent::__construct();
        $this->addData($arr);
    }
}