 DROP TABLE IF EXISTS `heroes`;

 CREATE TABLE `heroes` (
   `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
   `name` varchar(70) DEFAULT NULL,
   `email` varchar(255) DEFAULT NULL,
   `phone` varchar(10) DEFAULT NULL,
   PRIMARY KEY (`id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 LOCK TABLES `heroes` WRITE;
 /*!40000 ALTER TABLE `heroes` DISABLE KEYS */;

 INSERT INTO `heroes` (`id`, `name`, `email`, `phone`)
 VALUES
      (1,'Thanos','thanos@infgauntlet.com','5555555555'),
      (2,'Spider Man','peter.parker@thebugle.net','2125555555'),
      (3,'Daredevil','matt.murdock@nelsonandmurdock.org','2125555555'),
      (4,'The Maker','reed.richards@therealreedrichards.net','2125555555'),
      (5,'Rocket','rocket@starlordstinks.com','5555555555'),
      (6,'Galactus','galactus@worldeater.com','5555555555'),
      (7,'Silver Surfer','norrin.radd@zenn-la.gov','5555555555'),
      (8,'Hulk','bruce.banner@hulkout.org','2125555555'),
      (9,'Squirrel Girl','doreen.green@nannyservices.net','2125555555'),
      (10,'Thor','thor@odinson.gov','5555555555');

 /*!40000 ALTER TABLE `heroes` ENABLE KEYS */;
 UNLOCK TABLES;