<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */

require __DIR__ . '/../vendor/autoload.php';

/**
 * Class Book
 */
class Book extends \Ext\Data\Model
{
    /** @var array */
    protected $proxy = [
        'url' => 'users.json',
        'reader' => 'xml'
    ];

    /** @var array */
    protected $fields = [
        [
            'name' => 'Author',
            'mapping' => '@author.name'
        ],
        'Title',
        'Manufacturer',
        'ProductGroup',
        'DetailPageURL'
    ];

}

/** @var Book $model */
$model = new Book;

/** @var \Ext\Data\Store $store */
$store = new \Ext\Data\Store([
    'auto_load' => true,
    'proxy' => new \Ext\Data\Proxy\Ajax([
        'url' => './sheldon.xml',
        'reader' => new \Ext\Data\Reader\Xml([
            'record' => 'Item',
            'total_property' => 'total'
        ])
    ])
]);

$store->setModel($model);

/** @var \Ext\Grid\Panel $grid */
$grid = new \Ext\Grid\Panel([
    'buffered_renderer' => false,
    'columns' => [
        \Ext\Grid\Column\Column::factory([
            'text' => 'Author',
            'width' => 120,
            'data_index' => 'Author',
            'sortable' => true
        ]), [
            'text' => 'Title',
            'flex' => 1,
            'data_index' => 'Title',
            'sortable' => true
        ], [
            'text' => 'Manufacturer',
            'width' => 125,
            'data_index' => 'Manufacturer',
            'sortable' => true
        ], [
            'text' => 'Product Group',
            'width' => 125,
            'data_index' => 'ProductGroup',
            'sortable' => true
        ]
    ],
    'force_fit' => true,
    'height' => 210,
    'split' => true,
    'region' => 'north'
]);

$grid->setStore($store);

/** @var \Ext\Panel\Panel $panel */
$panel = new \Ext\Panel\Panel([
    'render_to' => 'binding-example',
    'frame' => true,
    'title' => 'Book List',
    'width' => 580,
    'height' => 400,
    'layout' => 'border',
    'items' => [
        $grid, [
            'id' => 'detailPanel',
            'region' => 'center',
            'body_padding' => 7,
            'body_style' => 'background: #ffffff;',
            'html' => 'Please select a book to see additional details.'
        ]
    ]
]);
?>

<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=10, user-scalable=yes">
    <title>Data Binding Example</title>

    <link rel="stylesheet"
          href="http://examples.sencha.com/extjs/6.5.1/examples/classic/shared/examples.css">

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/extjs/6.2.0/classic/theme-triton/resources/theme-triton-all.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.31.0/codemirror.min.css">

</head>
<body>

<h1>Data Binding Example</h1>
<p>This example expands upon the <a href="./kitchensink/?classic#xml-grid">XML Grid example</a> and shows how to
    implement data binding for a master-detail view.</p>
<p>Note that the js is not minified so it is readable.</p>

<div id="binding-example"></div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/extjs/6.2.0/ext-all.js"></script>
<script>

    Ext.require('Ext.panel.Panel');

    Ext.onReady(function () {
        Ext.create(<?= json_encode($panel->encode()); ?>);
    });

</script>

</body>
</html>