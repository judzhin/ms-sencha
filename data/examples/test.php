<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */

require __DIR__ . '/../vendor/autoload.php';

/**
 * Class MyClass
 */
// class MyClass extends \Ext\AbstractBase
// {
//
// }

///** @var MyClass $myClass */
//$myClass = new MyClass([
//    'first_name' => 'Judzhin',
//    'second_name' => 'Miles'
//]);

//echo $myClass->getFirstName();
//echo "\n";
//echo $myClass->getSecondName();
//echo "\n";
//echo $myClass->getClassName();

/** @var \Ext\Panel\Panel $class */
$class = \Ext\Ext::create(
    \Ext\Panel\Panel::class, [
        'title' => 'Some Panel Title',
        'items' => [
            [
                'xtype' => 'panel',
                'title' => 'Sone Inner Panel Title 1'
            ], [
                'xtype' => 'panel',
                'title' => 'Sone Inner Panel Title 2'
            ]
        ]
    ]
);

r($class);
die();

///** @var \Ext\Panel\Panel $class */
//$class = \Ext\Ext::create(
//    \Ext\Panel\Panel::class, [
//        'title' => 'Some Panel Title',
//        'html' => 'Some Panel HTML'
//    ]
//);