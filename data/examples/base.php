<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */

require __DIR__ . '/../vendor/autoload.php';

/**
 * Class MyClass
 */
class MyClass extends \Ext\AbstractBase
{

}

/** @var MyClass $myClass */
$myClass = new MyClass([
    'first_name' => 'Judzhin',
    'second_name' => 'Miles'
]);

echo $myClass->getFirstName();
echo "\n";
echo $myClass->getSecondName();
echo "\n";
echo $myClass->getClassName();
