<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
header('Content-Type: text/javascript');

// convert API config to Ext Direct spec
$API = include __DIR__ . '/config.php';
$actions = [];

/**
 * @var  $aname
 * @var  $a
 */
foreach ($API as $aname => &$a) {

    /** @var array $methods */
    $methods = [];

    foreach ($a['methods'] as $mname => &$m) {
        if (isset($m['len'])) {
            $md = [
                'name' => $mname,
                'len' => $m['len']
            ];
        } else {
            $md = [
                'name' => $mname,
                'params' => $m['params']
            ];
        }
        if (isset($m['formHandler']) && $m['formHandler']) {
            $md['formHandler'] = true;
        }
        $methods[] = $md;
    }

    $actions[$aname] = $methods;
}

/** @var array $cfg */
$cfg = [
    'url' => 'router.php',
    'type' => 'remoting',
    'actions' => $actions
];

echo 'var Ext = Ext || {}; Ext.REMOTING_API = ';
echo json_encode($cfg);
echo ';';