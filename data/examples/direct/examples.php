<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */

require __DIR__ . '/../../vendor/autoload.php';

?>

<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=10, user-scalable=yes">
    <title>Data Binding Example</title>

    <link rel="stylesheet"
          href="http://examples.sencha.com/extjs/6.5.1/examples/classic/shared/examples.css">

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/extjs/6.2.0/classic/theme-triton/resources/theme-triton-all.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.31.0/codemirror.min.css">

    <script src="/direct/api.php"></script>

</head>
<body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/extjs/6.2.0/ext-all-debug.js"></script>
<script>

    Ext.require('Ext.data.Store');
    Ext.require('Ext.data.proxy.Direct');

    Ext.onReady(function () {

        Ext.direct.Manager.addProvider(Ext.REMOTING_API);

        var store = Ext.create('Ext.data.Store', {
            fields: [
                'name', 'email', 'phone'
            ],

            proxy: {
                type: 'direct',
                directFn: "HeroesGateway.getResults"
            },

            autoLoad: true
        });

        console.log(store);
    });

</script>

</body>
