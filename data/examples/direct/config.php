<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */
return [
    'HeroesGateway' => [
        'methods' => [
            'getResults' => [
                'len' => 1
            ]
        ]
    ]
];