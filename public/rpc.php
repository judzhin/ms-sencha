<?php
/**
 * @access protected
 * @author Judzhin Miles <info[woof-woof]msbios.com>
 */

require __DIR__ . '/../vendor/autoload.php';

/** @var array $api */
$api = include __DIR__ . '/../config/autoload/api.global.php';

$api = $api[\Ext\Action\SmdAction::class];

/** @var \Zend\Db\Adapter\AdapterInterface $adapter */
$adapter = new \Zend\Db\Adapter\Adapter([
    'driver' => 'Pdo',
    'dsn' => 'mysql:dbname=examples.dev;host=127.0.0.1;charset=utf8',
    'username' => 'root',
    'password' => 'root'
]);

/** @var \Zend\Db\TableGateway\TableGatewayInterface $heroes */
$heroes = new \Zend\Db\TableGateway\TableGateway('heroes', $adapter);

/**
 * Class HeroesGateway
 */
class HeroesGateway
{
    /** @var  \Zend\Db\TableGateway\TableGatewayInterface */
    protected $tableGateway;

    /**
     * HeroesGateway constructor.
     * @param \Zend\Db\TableGateway\TableGatewayInterface $tableGateway
     */
    public function __construct(\Zend\Db\TableGateway\TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function getResults($params)
    {
        return $this->tableGateway->select()->toArray();
    }
}

/** @var HeroesGateway $heroesTableGateway */
$heroesTableGateway = new HeroesGateway($heroes);

/**
 * Class Bogus
 */
class Bogus
{
    public $action;
    public $method;
    public $data;
    public $tid;
}

$isForm = false;
$isUpload = false;

/** @var array $rawdata */
$rawdata = file_get_contents('php://input');

if (!empty($rawdata)) {

    header('Content-Type: text/javascript');
    $data = json_decode($rawdata);

} else if (isset($_POST['action'])) { // form post

    $isForm = true;
    $isUpload = $_POST['extUpload'] == 'true';
    $data = new Bogus;
    $data->action = $_POST['extAction'];
    $data->method = $_POST['extMethod'];
    $data->tid = isset($_POST['extTID']) ? $_POST['extTID'] : null;
    $data->data = [$_POST, $_FILES];
} else {

    die('Invalid request.');
}


/**
 * @param $cdata
 * @return array
 */
$doRpc = function ($cdata) use ($api, $heroesTableGateway) {

    /** @var array $API */
    $API = $api;

    try {

        if (!isset($API[$cdata->action])) {
            throw new Exception('Call to undefined action: ' . $cdata->action);
        }

        /** @var string $action */
        $action = $cdata->action;

        /** @var string $a */
        $a = $API[$action];

        $method = $cdata->method;
        $mdef = $a['methods'][$method];

        if (!$mdef) {
            throw new Exception("Call to undefined method: $method " .
                "in action $action");
        }

        /** @var array $r */
        $r = [
            'type' => 'rpc',
            'tid' => $cdata->tid,
            'action' => $action,
            'method' => $method
        ];

        // require_once("classes/$action.php");
        // $o = new $action();
        $o = $heroesTableGateway;

        if (isset($mdef['len'])) {
            $params = isset($cdata->data) && is_array($cdata->data) ? $cdata->data : array();
        } else {
            $params = [$cdata->data];
        }

        // array_push($params, $cdata->metadata);

        $r['result'] = call_user_func_array([$o, $method], $params);
    } catch (Exception $e) {
        $r['type'] = 'exception';
        $r['message'] = $e->getMessage();
        $r['where'] = $e->getTraceAsString();
    }

    return $r;
};

$response = null;

if (is_array($data)) {

    /** @var array $response */
    $response = [];

    /** @var mixed $d */
    foreach ($data as $d) {
        $response[] = $doRpc($d);
    }

} else {

    /** @var array $response */
    $response = $doRpc($data);
}

if ($isForm && $isUpload) {
    echo '<html><body><textarea>';
    echo json_encode($response);
    echo '</textarea></body></html>';
} else {
    echo json_encode($response);
}