<?php

// Delegate static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server'
    && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))
) {
    return false;
}

chdir(dirname(__DIR__));
require 'vendor/autoload.php';

/**
 * Calculator - sample class to expose via JSON-RPC
 */
class Person
{
    /**
     * Return sum of two variables
     *
     * @param  int $x
     * @param  int $y
     * @return int
     */
    public function find($x, $y)
    {
        return $x + $y;
    }

    /**
     * @return bool
     */
    public function search ()
    {
        return true;
    }

}

class User
{
    /**
     * Return sum of two variables
     *
     * @param  int $x
     * @param  int $y
     * @return int
     */
    public function find($x, $y)
    {
        return $x + $y;
    }
}

try {

    // /** @var \Ext\Json\Server\Smd\Collection $collection */
    // $collection = new \Ext\Json\Server\Smd\Collection();
    // $collection->setClass(Person::class);

    /** @var \Zend\Json\Server\Server $server */
    $server = new \Ext\Json\Server\Server;
    $server->setClass(Person::class);
    // $server->setClass(User::class);

    $server->setTarget('/json-rpc.php');
    $server->setEnvelope(\Ext\Json\Server\Smd::ENV_JSONRPC_2);

    $smd = $server->getServiceMap();

    // header('Content-Type: application/json');
    echo $smd;
} catch (Exception $exc) {
    echo $exc->getMessage();
}
